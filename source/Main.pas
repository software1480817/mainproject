unit Main;

interface

uses
  packages.tools.VersionInformation,
  Info,
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Menus;

type
  TRepairDFM = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    bOpen: TButton;
    bRepairandSave: TButton;
    mmoInfo: TMemo;
    bDebugButton: TButton;
    mmMenu: TMainMenu;
    Info1: TMenuItem;
    procedure bOpenClick(Sender: TObject);
    procedure bRepairandSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bDebugButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Info1Click(Sender: TObject);

  private
    fContent:  TStringList;
    fFileName: string;
    fInfoRecord: TExtractFileVersionData;
    fDeveloperVersionData: TDeveloperVersionData;

    procedure Info(const AMsg: string);
    procedure Repair;
    procedure EnableOpen(const AValue: Boolean);
    procedure openFile(aFileName: string);

    { Private-Deklarationen }
  public
    procedure AfterConstruction; override;
  end;

  // returning Version information from gitversion.inc
  function GetVersionInformation: TExtractFileVersionData;
  function GetVersionNumber: String;


var
  RepairDFM: TRepairDFM;

implementation

{$R *.dfm}

function GetVersionInformation: TExtractFileVersionData;
begin
  Result := VersionInformation.ExtractInformations; // exceptions can be raised!!
end;

function GetVersionNumber: string;
begin
  Result := GetVersionInformation.FileVersionData.FileVersionNr;
end;


procedure TRepairDFM.FormCreate(Sender: TObject);
begin
  fContent := TStringList.Create;
  EnableOpen(true);
end;

procedure TRepairDFM.EnableOpen(const AValue: Boolean);
begin
  bOpen.Enabled := AValue;
  bRepairandSave.Enabled := not AValue;
end;

procedure TRepairDFM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fContent.free;
end;

procedure TRepairDFM.Info(const AMsg: string);
begin
  mmoInfo.Lines.Add(AMsg);
end;

procedure TRepairDFM.bOpenClick(Sender: TObject);
var
  vOpenDialog: TOpenDialog;
begin
  mmoInfo.Lines.Clear;
  fContent.Clear;
  try
    vOpenDialog := TOpenDialog.Create(self);
    try
      vOpenDialog.Options := [ofFileMustExist];
      vOpenDialog.Filter := 'Delphi dproj files|*.dproj';

      if vOpenDialog.Execute then
        openFile(vOpenDialog.FileName)
      else
      begin
        EnableOpen(true);
        Info('Datei �ffnen angebrochen.');
      end;

    finally
      vOpenDialog.free;
    end;
  except
    on E: Exception do
    begin
      Info(E.Message);
      EnableOpen(true);
    end;
  end;

end;

procedure TRepairDFM.bRepairandSaveClick(Sender: TObject);
begin
  Repair;
end;

procedure TRepairDFM.AfterConstruction;
begin
  inherited;

 {$I gitversion.inc}

  fDeveloperVersionData.LASTUPDATE := LASTUPDATE;
  fDeveloperVersionData.GIT_LAST_TAG := GIT_LAST_TAG;
  fDeveloperVersionData.GIT_LAST_HASH := GIT_LAST_HASH;
  fDeveloperVersionData.GIT_CURRENT_BRANCH := GIT_CURRENT_BRANCH;
  fDeveloperVersionData.GIT_AUTHOR_NAME := GIT_AUTHOR_NAME;
  fDeveloperVersionData.GIT_COMMIT_DATE := GIT_COMMIT_DATE;

  try // exception can be raised
    fInfoRecord := VersionInformation.ExtractInformations;
  except
  end;
  if VersionInformation.IsRelease then
    Self.Caption := Self.Caption + ' - V' + fInfoRecord.FileVersionData.FileVersionNr
  else
    Self.Caption := Self.Caption + ' - ' + 'Entwicklerversion' + ' | ' + fDeveloperVersionData.GIT_CURRENT_BRANCH + ' | ' + fDeveloperVersionData.GIT_AUTHOR_NAME

end;

procedure TRepairDFM.Info1Click(Sender: TObject);
var
  vFormInfo: TFormInfo;
begin
  vFormInfo := TFormInfo.Create(Self, fInfoRecord, fDeveloperVersionData);
  vFormInfo.ShowModal;
  vFormInfo.Free;
end;

procedure TRepairDFM.openFile(aFileName: string);
var
  vFileStream: TFileStream;
begin
  EnableOpen(false);
  fFileName := aFileName;
  Info('lade Datei: ' + fFileName);
  vFileStream := TFileStream.Create(fFileName, fmShareDenyNone);
  try
    fContent.LoadFromStream(vFileStream);
  finally
    vFileStream.Free;
  end;
end;

procedure TRepairDFM.bDebugButtonClick(Sender: TObject);
var
  vFileStream: TFileStream;
begin
  EnableOpen(false);
  mmoInfo.Lines.Clear;
  fContent.Clear;
  fFileName := 'C:\Development\tools\RepairDFM\Test\OurPlantOS.dproj';
  vFileStream := TFileStream.Create(fFileName, fmShareDenyNone);
  fContent.LoadFromStream(vFileStream);
  vFileStream.Destroy();
  Repair;
end;

procedure TRepairDFM.FormShow(Sender: TObject);
var
  vProjDFM: string;
begin
  if ParamCount = 1 then
  begin
    vProjDFM := ParamStr(1);
    Info(Format('parameter received on startup: "%s"', [vProjDFM]));
    if FileExists(vProjDFM) then
    begin
      openFile(vProjDFM);
      Repair;
      Sleep(1000);
      Close;
    end;
  end;
end;


const
  FORMTAG          = '<FORM>';
  FORMTYPE         = '<FORMTYPE>';
  ADDFORMTYPE      = '<FormType>dfm</FormType>';
  ADDFORMTYPESPACE = '            ';

procedure TRepairDFM.Repair;
var
  i:                    Integer;
  vFormTagCount:        Integer;
  vFormTypeCount:       Integer;
  vFormTypeRepairCount: Integer;
  vNextLine:            string;
  vResultContent:       TStringList;
begin
  vFormTagCount := 0;
  vFormTypeCount := 0;
  vFormTypeRepairCount := 0;
  try
    vResultContent := TStringList.Create;
    try
      for i := 0 to fContent.Count - 2 do
      // nicht bis zur letten zeile..das reicht, damit kann ich i+1 testen
      begin
        if fContent[i].ToUpper.Contains(FORMTAG) then
        begin
          inc(vFormTagCount);
          vResultContent.Add(fContent[i]);
          // hat die n�chste Zeile <FormType> ?
          vNextLine := fContent[i + 1];
          if vNextLine.ToUpper.Contains(FORMTYPE) then
          begin
            inc(vFormTypeCount);
          end
          else
          begin
            inc(vFormTypeRepairCount);
            vResultContent.Add(ADDFORMTYPESPACE + ADDFORMTYPE);
          end;
        end
        else
          vResultContent.Add(fContent[i]);
      end;

      // und jetzt noch das letzte...
      vResultContent.Add(fContent[fContent.Count - 1]);

      Info(FORMTAG + ' ' + IntToStr(vFormTagCount) + ' x gefunden.');
      Info(FORMTYPE + ' ' + IntToStr(vFormTypeCount) + ' x gefunden.');
      Info(ADDFORMTYPE + ' ' + IntToStr(vFormTypeRepairCount) + ' x vermisst und repariert.');
      Info('schreibe Datei neu.');

      vResultContent.SaveToFile(fFileName, TEncoding.UTF8);

    finally
      vResultContent.free;
      EnableOpen(true);
    end;
  except
    on E: Exception do
      Info(E.Message);
  end;

end;

end.
