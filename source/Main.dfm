object RepairDFM: TRepairDFM
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Repair DFM'
  ClientHeight = 287
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mmMenu
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 9
    Width = 348
    Height = 13
    Caption = 
      'Die DFM Struktur in einer dproj wird sehr oft zerst'#246'rt: <FormTyp' +
      'e> fehlt'
  end
  object Label2: TLabel
    Left = 10
    Top = 25
    Width = 178
    Height = 13
    Caption = 'Dieses Programm stellt sie wieder her'
  end
  object Label3: TLabel
    Left = 10
    Top = 49
    Width = 136
    Height = 13
    Caption = '<Form>meinFrame</Form>'
  end
  object Label4: TLabel
    Left = 18
    Top = 65
    Width = 178
    Height = 13
    Caption = '<FormType>dfm</FormType>'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 10
    Top = 81
    Width = 186
    Height = 13
    Caption = '<DesignClass>TFrame</DesignClass>'
  end
  object bOpen: TButton
    Left = 10
    Top = 108
    Width = 140
    Height = 49
    Caption = #246'ffne dproj'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = bOpenClick
  end
  object bRepairandSave: TButton
    Left = 216
    Top = 108
    Width = 140
    Height = 49
    Caption = 'reparieren und speichern'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = bRepairandSaveClick
  end
  object mmoInfo: TMemo
    Left = 10
    Top = 170
    Width = 345
    Height = 105
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object bDebugButton: TButton
    Left = 256
    Top = 28
    Width = 75
    Height = 74
    Caption = 'bDebugButton'
    TabOrder = 3
    Visible = False
    OnClick = bDebugButtonClick
  end
  object mmMenu: TMainMenu
    Left = 208
    Top = 200
    object Info1: TMenuItem
      Caption = 'Info'
      OnClick = Info1Click
    end
  end
end
