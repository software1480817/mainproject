// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program MyMainPrj;

uses
  Vcl.Forms,
  Main in '..\..\source\Main.pas' {RepairDFM},
  Info in '..\..\source\Info.pas' {FormInfo};

{$R version.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TRepairDFM, RepairDFM);
  Application.CreateForm(TFormInfo, FormInfo);
  Application.Run;
end.
